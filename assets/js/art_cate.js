// 添加列表
function getCateList() {
    $.get(
        '/my/article/cates',
        function (res) {
            console.log(res);
            if (res.status != 0) {
                return layer.msg('获取图书列表失败')
            }
            var str = template('tpl', res)
            $('tbody').html(str)

        }
    )
}
var index;
$(function () {
    getCateList()
    // 添加分类
    $('.add').click(function () {
        index = layer.open({
            area: ['500px', '300px'],
            title: '添加分类',
            content: $('#dialog-add').html()
        })
    })
})

// 添加分类
$('body').on('submit', '#form-add', function (e) {
    e.preventDefault();
    $.ajax({
        method: 'post',
        url: '/my/article/addcates',
        data: $(this).serialize(),
        success: function (res) {
            if (res.status != 0) {
                return layer.msg('添加分类失败')
            }
            getCateList();
            //⭐⭐⭐ 将layer.open赋值给index，然后index必须是一个全局变量，然后当我们在这里调用完layer.open后，手动将layer.open关闭
            layer.close(index)
        }
    })
})

// 编辑功能第一步
var editIndex;
$('tbody').on('click', '.btn-edit', function () {
    editIndex = layer.open({
        type: '1',
        area: ['500px', '220px'],
        title: '添加分类',
        content: $('#dialog-edit').html()
    });

    // 编辑功能第二步  发起Ajax请求
    var id = $(this).attr('data-id');
    $.ajax({
        method: 'get',
        url: '/my/article/cates/' + id,
        success: function (res) {
            if (res.status != 0) return layer.msg('获取数据失败！')
            console.log(res);
            console.log(res.data);
            layui.form.val('form-edit', res.data)
        }
    })

})

// 保存编辑的信息   ⭐模板引擎  只能用事件委托绑定事件
$('body').on('submit', '#form-edit', function (e) {
    e.preventDefault();
    $.ajax({
        method: 'post',
        url: '/my/article/updatecate',
        data: $(this).serialize(),
        success: function (res) {
            if (res.status != 0) return layer.msg('更新分类失败')
            layer.msg('更新成功')
            // 更新数据 渲染
            getCateList()
            // 关闭弹出层
            layer.close(editIndex)
        }
    })
})

// 删除功能
$('tbody').on('click', '.btn-del', function () {
    var id = $(this).attr('data-id');
    // 删除最好是和用户确认一下
    layer.confirm('要确认删除吗?', { icon: 3, title: '提示' }, function (index) {
        //do something
        $.ajax({
            method: 'get',
            url: '/my/article/deletecate' + id,
            success: function (res) {
                if (res.status != 0) return layer.msg('删除失败')
                // 删除成功  书信数据
                getCateList()
            }
        })
        layer.close(index);
    });
})