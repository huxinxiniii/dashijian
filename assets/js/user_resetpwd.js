layui.form.verify({
    pwd: [/^[\S]{6,16}$/, '密码不能为空，且必须为6~16位'],
    samePwd: function (value) {
        if (value === $('[name=oldPwd]').val()) {
            return '新旧密码不能相同！'
        }
    },
    repwd: function (value) {
        // ⭐⭐value是什么？规则放在谁身上，默认就是谁的value  这里是repwd的value
        if ($('[name=newPwd]').val() != value) {
            return '两次密码不一致！'
        }
    }
})

$('.layui-form').on('submit', function (e) {
    e.preventDefault()
    $.ajax({
        method: 'POST',
        url: '/my/updatepwd',
        data: $(this).serialize(),
        success: function (res) {
            if (res.status !== 0) {
                return layui.layer.msg('更新密码失败！')
            }
            layui.layer.msg('更新密码成功！')
            // 重置表单
            $('.layui-form')[0].reset()
        }
    })
})