function getUserInfo() {
    $.ajax({
        type: 'get',
        url: '/my/userinfo',
        success: function (res) {
            // console.log(res);
            if (res.status != 0) {
                // 弹出提示框
                return layer.msg('获取用户信息失败')
            }
            rederAvatar(res.data)
        },
        // ⭐⭐不管成功与否 Ajax都会调用complete函数
        // 在则会里面判断是否有token是最保险的
        // complete: function (res) {
        //     // console.log(res);
        //     // responseJSON是complete的一个属性
        //     if (res.responseJSON.status != 0) {
        //         // 只要出问题了  直接调回登录页
        //         localStorage.removeItem('token')
        //         location.href = '/login.html'

        //     }
        // }
    })
}
// 如果有昵称就渲染昵称 没有昵称就渲染用户名
// 如果有头像就渲染头像 没有就把用户名瘦子父母做头像
function rederAvatar(userInfo) {
    // console.log(userInfo.username);
    // console.log(typeof userInfo.username);
    if (userInfo.nickname) {
        $('#welcome').html('欢迎' + userInfo.nickname)
    } else {
        $('#welcome').html('欢迎' + userInfo.username)
    }
    if (userInfo.user_pic) {
        $('.userinfo .layui-nav-img').attr('src', userInfo.user_pic);
        $('.userinfo .text-avatar').hide()
    } else {
        $('.userinfo .text-avatar').text(userInfo.username.slice(0, 1).toUpperCase())
        // $('.userinfo .text-avatar').text(userInfo.username[0].toUpperCase())
        $('.userinfo .layui-nav-img').hide()
    }
}


//获取token

getUserInfo()

// 实现退出的功能
$('#logout').click(function () {
    layer.confirm('确定要退出吗？', { icon: 3, title: '提示' }, function (index) {
        // 销毁凭证
        localStorage.removeItem('token')
        // 跳转回登录页
        location.href = '/login.html';
        // 关闭询问框
        layer.close(index)

    })
})




