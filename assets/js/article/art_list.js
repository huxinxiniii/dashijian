var q = {
    pagenum: 1,//	是	int	页码值
    pagesize: 2,//	是	int	每页显示多少条数据
    cate_id: '', //否	string	文章分类的 Id  ⭐非必需参数
    state: '',//文章的状态 已发布 草稿  ⭐非必需参数
}


// 渲染列表
function initTable() {
    // 发送请求 获取数据
    $.ajax({
        method: 'GET',
        url: '/my/article/list',
        data: q,
        success: function (res) {
            if (res.status != 0) return layer.msg("请求列表失败")
            console.log(res);
            var str = template('tpl', res);
            $('tbody').html(str)
            renderPage(res.total)
        }
    })
}
initTable();

// 获取文章分类数据
function getCateList() {
    $.ajax({
        method: 'get',
        url: '/my/article/cates',
        success: function (res) {
            if (res.status != 0) return layer.msg('获取列表失败');
            console.log(res);
            var str = template('tpl-cate', res)
            $('[name=cate_id]').html(str);
            layui.form.render()// ⭐⭐⭐数据时动态后面添加的，layui的表单需要更新一下
        }
    })
}
getCateList()


// 筛选功能
$('#form-search').submit(function (e) {
    e.preventDefault();
    // 更新q中的数据 然后渲染
    q.cate_id = $('[name=cate_id]').val();
    q.state = $('[name=state]').val();
    initTable();
})


function renderPage(total) {
    // layui的分页函数
    layui.laypage.render({
        elem: 'page-box', //注意，这里的 test1 是 ID，不用加 # 号
        count: total, //数据总数，从服务端得到
        limit: q.pagesize,//每页显示的个数
        curr: q.pagenum,//当前页
        limits: [2, 3, 4, 5],  //下拉栏调整每页显示的页数
        layout: ['count', 'limit', 'prev', 'page', 'next'], //调整以上几项的顺序
        jump: function (obj, first) {  // obj当分页被切换时触发   first（是否首次，一般用于初始加载的判断）
            console.log(obj.curr);  //显示当前切换过来的页码
            console.log(first);
            q.pagenum = obj.curr;
            q.pagesize = obj.limit;
            if (!first) {
                initTable();
            }
        }
    });
}


// 删除功能
$('tbody').on('click', '.btn-del', function () {
    var id = $(this).attr('data-id');
    // 删除最好是和用户确认一下
    layer.confirm('要确认删除吗?', { icon: 3, title: '提示' }, function (index) {
        //do something
        $.ajax({
            method: 'get',
            url: '/my/article/delete/' + id,
            success: function (res) {
                if (res.status != 0) return layer.msg('删除失败')
                layer.msg('删除成功')
                // 删除成功  刷新数据 如果删除的是唯一的一条 页码-1
                if ($('.btn-del').length == 1) {
                    q.pagenum = (q.pagenum == 1 ? 1 : q.pagenum - 1)
                }
                getCateList()
            }
        })
        layer.close(index);
    });
})

// 编辑功能
$('tbody').on('click', '.btn-edit', function () {
    // var id = $(this).attr('data-id');
    // 删除最好是和用户确认一下
    layer.confirm('要确认编辑吗?', { icon: 3, title: '提示' }, function (index) {
        //do something
        // $.ajax({
        //     method: 'get',
        //     url: '/my/article/deletecate' + id,
        //     success: function (res) {
        //         if (res.status != 0) return layer.msg('删除失败')
        //         // 删除成功  书信数据
        //         getCateList()
        //     }
        // })
        layer.close(index);
    });
})
