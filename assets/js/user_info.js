function initUserInfo() {
    $.ajax({
        method: 'get',
        url: '/my/userinfo',
        success: function (res) {
            if (res.status != 0) {
                return layer.msg('获取用户信息失败')
            }
            console.log(res);
            // 动态获取数据渲染
            layui.form.val('formUserInfo', res.data)
        }
    })
}
initUserInfo()

// 充值信息功能
$('#reset').click(function () {
    initUserInfo()
})

// 提交信息功能
$('.layui-form').submit(function (e) {
    e.preventDefault();
    $.ajax({
        type: 'post',
        url: '/my/userinfo',
        data: $(this).serialize(),
        success: function (res) {
            if (res.status != 0) {
                layer.msg('更新信息失败')
            }
            console.log('更新信息成功')
            console.log(window.parent)
            window.parent.getUserInfo()

        }
    })
})

