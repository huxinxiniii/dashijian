function getCateList() {
    $.ajax({
        method: 'GET',
        url: '/my/article/cates',
        success: function (res) {
            if (res.status != 0) return layer.msg('获取数据失败')
            console.log(res);
            var str = template('tpl', res)
            $('[name="cate_id"]').html(str);
            // 异步操作  页面打开时渲染出来的是空的
            layui.form.render()
        }
    })
}
getCateList()

// ⭐初始化富文本编辑器⭐
// 初始化富文本编辑器
initEditor()

// 1. 初始化图片裁剪器
var $image = $('#image')

// 2. 裁剪选项
var options = {
    aspectRatio: 400 / 280,
    preview: '.img-preview'
}

// 3. 初始化裁剪区域
$image.cropper(options)


// 点给我们点击按钮的时候 模拟点击文件选择框
$('#btn').click(function () {
    $("#coverFile").click()
})

// 更换裁剪的图片
$("#coverFile").on('change', function (e) {
    // console.log(e.target.files);
    // 拿到用户选择的图片
    var file = e.target.files[0];
    // 根据选择的图片生成地址
    var newImgURL = URL.createObjectURL(file);
    $image
        .cropper('destroy')      // 销毁旧的裁剪区域
        .attr('src', newImgURL)  // 重新设置图片路径
        .cropper(options)        // 重新初始化裁剪区域
})

// 文章状态
var art_state = '已发布';
$('#btnSave1').on('click', function () {
    art_state = '已发布'  // 发布事件   
})
$('#btnSave2').on('click', function () {  // 存为草稿事件
    art_state = '存为草稿';
})

$('#form-pub').on('submit', function (e) {
    //参数  请求体（FormData 格式）
    e.preventDefault()
    // 基于 form 表单，快速创建一个 FormData 对象
    var fd = new FormData(this)
    fd.append('state', art_state)
    fd.forEach(function (v, k) {
        console.log(v, k);
    })
    $image
        .cropper('getCroppedCanvas', {
            // 创建一个 Canvas 画布
            width: 400,
            height: 280
        })
        .toBlob(function (blob) {
            // 将 Canvas 画布上的内容，转化为文件对象
            // 得到文件对象后，进行后续的操作
            // 5. 将文件对象，存储到 fd 中
            fd.append('cover_img', blob)
            // 6. 发起 ajax 数据请求
            publishArticle(fd)
        })
})

function publishArticle(formData) {

    $.ajax({
        method: 'post',
        url: '/my/article/add',
        data: formData,
        // ⭐Ajax会默认帮我们把数据转换成key=value&key=value形式，我们通过这个属性
        processData: false,
        contentType: false,
        success: function (res) {
            if (res.status != 0) return layer.msg('添加失败')
            location.herf = '/assets/article/art_list.html'
        }
    })
}
