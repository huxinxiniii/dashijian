
// ⭐⭐这个函数会在 
// 在所有参数选项被jQuery.ajax()函数处理之前，你可以使用该函数设置的回调函数来预先更改任何参数选项。
$.ajaxPrefilter(function (options) {
    // 判断url是不是以my开头的，根据这个来指定header
    if (options.url.startsWith('/my')) {
        options.headers = {
            Authorization: localStorage.getItem('token')
        }
        options.complete = function (res) {
            // console.log(res);
            // res.responseJSON是complete的一个属性
            if (res.responseJSON.status === 1 && res.responseJSON.message === '身份认证失败！') {
                // 只要出问题了  直接调回登录页
                localStorage.removeItem('token')
                location.href = '/login.html'
            }
        }
    }
    options.url = 'http://ajax.frontend.itheima.net' + options.url;

























})