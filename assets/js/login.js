// 点击的时候在注册  登录之间切换
$('#link_reg').on('click', function () {
    $('.login-box').hide()
    $('.reg-box').show()
})
$('#link_login').on('click', function () {
    $('.reg-box').hide()
    $('.login-box').show()
})


// 从layui获取form对象
var form = layui.form;
var layer = layui.layer;
var myurl = 'http://ajax.frontend.itheima.net'

//通过form.verify()自定义校验规则
form.verify({
    // 自定义了一个叫pwd的校验规则
    pwd: [/^[\S]{6,12}$/, '密码必须6~12位，且不能出现空格'],
    repwd: function (value) {
        if ($('.reg-box [name="password"]').val() != value) {
            return '两次密码不一致'
        }
    }
})


// 注册
$('#form_reg').submit(function (e) {
    e.preventDefault();
    let mydata = {
        username: $('.reg-box [name=username]').val(),
        password: $('.reg-box [name=password]').val()
    };

    $.post(
        '/api/reguser', mydata,
        function (res) {
            if (res.status == 1) return layer.msg(res.message);
            layer.msg('注册成功');
            $('#link_login').click();
        }
    )
})

// 登录
$('#form_login').submit(function (e) {
    e.preventDefault();
    $.post(
        '/api/login',
        $(this).serialize(),
        function (res) {
            if (res.status != 0) return layer.msg(res.message);
            // 把凭证存到本地存储
            localStorage.setItem('token', res.token)
            // 调到后台首页  页面跳转
            location.href = '/index.html'
        }
    )
})
